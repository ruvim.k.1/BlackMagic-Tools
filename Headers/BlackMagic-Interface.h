#include <windows.h> 

DWORD printText (const char * s, unsigned int l) { 
	DWORD bWritten = 0; 
	WriteFile (GetStdHandle (STD_OUTPUT_HANDLE), s, l, &bWritten, 0); 
	return bWritten; 
} 
DWORD print (const char * s) { 
	return printText (s, strlen (s)); 
} 
DWORD printHex (long long iValue) { 
	char hex [16] = { '0' }; 
	char * p = &hex[15]; 
	long long v = iValue; 
	while (p >= hex) { 
		char c = v & 0x0F; 
		if (c || v) 
			*p = c >= 0x0A ? 
				c - 0x0A + 'A' 
				: c + '0'; 
		else *p = ' '; 
		p--; 
		v = v >> 4; 
	} 
	DWORD bWritten = 0; 
	WriteFile (GetStdHandle (STD_OUTPUT_HANDLE), hex, 16, &bWritten, 0); 
	return bWritten; 
} 
DWORD printDec (long iValue) { 
	char hex [10] = { '0' }; 
	char * p = &hex[9]; 
	long long v = iValue; 
	while (p >= hex) { 
		char c = v % 10; 
		if (c || v) 
			*p = c + '0'; 
		else *p = ' '; 
		p--; 
		v = v / 10; 
	} 
	DWORD bWritten = 0; 
	WriteFile (GetStdHandle (STD_OUTPUT_HANDLE), hex, 10, &bWritten, 0); 
	return bWritten; 
} 

DWORD WINAPI bm_init (); 
void WINAPI bm_close (); 

DWORD WINAPI bm_restart (); 
DWORD WINAPI bm_check (); 

BOOL WINAPI bm_set_program_input (long long iInput); 
BOOL WINAPI bm_set_preview_input (long long iInput); 
BOOL WINAPI bm_get_program_input (long long * lpInput); 
BOOL WINAPI bm_get_preview_input (long long * lpInput); 
BOOL WINAPI bm_is_in_transition (); 

void * WINAPI bm_get_input (unsigned long input_index); 
HRESULT WINAPI bm_get_input_type (void * input, DWORD * type); 
DWORD WINAPI bm_set_aux_outputs (unsigned long aux_start_index, unsigned long aux_stop_index, long long input_id, unsigned long max_retries_per_aux); 
DWORD WINAPI bm_set_media_player_source (unsigned long mp_start_index, unsigned long mp_stop_index, unsigned long media_source_index, unsigned long max_retries); 

void * WINAPI bm_get_mix_block (); 
void * WINAPI bm_get_key (); 

void WINAPI bm_free (void * object); 

void WINAPI ruvim_init (const char * log_filename, BOOL log_append); 
void WINAPI ruvim_cleanup (); 


#define INPUT_1 		0x001 
#define INPUT_2 		0x002 
#define INPUT_3 		0x003 
#define INPUT_4 		0x004 
#define INPUT_5 		0x005 
#define INPUT_6 		0x006 
#define INPUT_7 		0x007 
#define INPUT_8 		0x008 
#define INPUT_9 		0x009 
#define INPUT_10		0x00A 

#define INPUT_BLK		0x000 
#define INPUT_COL1		0x7D1 
#define INPUT_BARS		0x3E8 
#define INPUT_MP1		0xBC2 
#define INPUT_MP2		0xBCC 

#define INPUT_PROGRAM	0x271A 
#define INPUT_PREVIEW	0x271B 

#define MOD_ALT 		0x0001 
#define MOD_CTRL		0x0002 
#define MOD_SHIFT		0x0004 
#define MOD_WIN 		0x0008 
#define MOD_NOREPEAT	0x4000 



