#include <windows.h> 

#define DEBUG 0 

#include <thread-search.h> 
#include <BlackMagic-Interface.h> 



void registerHotkeys (); 
void unregisterHotkeys (); 

DWORD WINAPI changeAux12 (LPVOID lpParam); 
DWORD WINAPI changeMP1 (LPVOID lpParam); 


#define ID_MP1 17 
#define ID_MP2 18 
#define ID_BLK 19 
#define ID_COL1 20 
#define ID_BARS 21 
#define ID_AUX_FREEZE 22 
#define ID_AUX_PC 23 
#define ID_AUX_PG 24 
#define ID_AUX_M1 25 
#define ID_AUX_M2 26 
#define ID_MAKE_WELCOME 27 
#define ID_MAKE_GOODBYE 28 
void registerHotkeys () { 
	RegisterHotKey (NULL, ID_MP1, MOD_NOREPEAT, 'J'); 
	RegisterHotKey (NULL, ID_MP2, MOD_NOREPEAT, 'K'); 
	RegisterHotKey (NULL, ID_BLK, MOD_NOREPEAT, 'D'); 
	RegisterHotKey (NULL, ID_COL1, MOD_NOREPEAT, 'R'); 
	RegisterHotKey (NULL, ID_BARS, MOD_NOREPEAT, 'A'); 
	RegisterHotKey (NULL, ID_AUX_FREEZE, MOD_NOREPEAT, 'Y'); 
	RegisterHotKey (NULL, ID_AUX_PC, MOD_NOREPEAT, 'U'); 
	RegisterHotKey (NULL, ID_AUX_PG, MOD_NOREPEAT, 'I'); 
	RegisterHotKey (NULL, ID_AUX_M1, MOD_NOREPEAT, 'O'); 
	RegisterHotKey (NULL, ID_AUX_M2, MOD_NOREPEAT, 'P'); 
	RegisterHotKey (NULL, ID_MAKE_WELCOME, MOD_NOREPEAT, 'W'); 
	RegisterHotKey (NULL, ID_MAKE_GOODBYE, MOD_NOREPEAT, 'S'); 
} 
void unregisterHotkeys () { 
	UnregisterHotKey (NULL, ID_MP1); 
	UnregisterHotKey (NULL, ID_MP2); 
	UnregisterHotKey (NULL, ID_BLK); 
	UnregisterHotKey (NULL, ID_COL1); 
	UnregisterHotKey (NULL, ID_BARS); 
	UnregisterHotKey (NULL, ID_AUX_FREEZE); 
	UnregisterHotKey (NULL, ID_AUX_PC); 
	UnregisterHotKey (NULL, ID_AUX_PG); 
	UnregisterHotKey (NULL, ID_AUX_M1); 
	UnregisterHotKey (NULL, ID_AUX_M2); 
	UnregisterHotKey (NULL, ID_MAKE_WELCOME); 
	UnregisterHotKey (NULL, ID_MAKE_GOODBYE); 
} 

DWORD enableTimer () { 
	return SetTimer (NULL, 0, 250, (void *) NULL); 
} 
void disableTimer (DWORD timerId) { 
	KillTimer (NULL, timerId); 
} 


int main (int argc, char * argv []) { 
	BOOL isRestart = killProcessByName ("BlackMagic-AdditionalHotkeys.exe", FALSE); 
	const char * procName = "ATEM Software Control.exe"; 
	const char * atemPath = "C:\\Program Files (x86)\\Blackmagic Design\\Blackmagic ATEM Switchers\\ATEM Software Control\\ATEM Software Control.exe"; 
	const char * cwdPath = "C:\\Program Files (x86)\\Blackmagic Design\\Blackmagic ATEM Switchers\\ATEM Software Control\\"; 
	STARTUPINFO atemStartInfo; 
	for (unsigned int i = 0; i < sizeof (STARTUPINFO); i++) 
		((char *)(&atemStartInfo)) [i] = 0; 
	atemStartInfo.cb = sizeof (STARTUPINFO); 
	PROCESS_INFORMATION atemProcInfo; 
	if (!killProcessByName (procName, TRUE) && // <-- checks if ATEM is already started. 
			!CreateProcess (atemPath, (char *) NULL, NULL, NULL, FALSE, 0, NULL, cwdPath, &atemStartInfo, &atemProcInfo)) { 
		DWORD lastError = GetLastError (); 
		print ("Error starting ATEM switcher. \r\nLast error: "); 
		printHex (lastError); 
		print ("\r\n"); 
	} 
	ruvim_init ("Ruvims-MediaKeys-log.txt", isRestart); 
	if (bm_init ()) { 
		print ("Error initializing BlackMagic interface. "); 
		ruvim_cleanup (); 
		return 1; 
	} 
	long long input = 0; 
	unsigned long output = 0; // 0 = PGM, 1 = AUX1&2; 
	MSG msg; 
	BOOL keepRunning = TRUE; 
	BOOL hkRegistered = FALSE; 
	DWORD timer = enableTimer (); 
	if (!timer) { 
		DWORD lastError = GetLastError (); 
		print ("Error creating a timer. Last error: "); 
		printHex (lastError); 
		print ("\r\n\r\n"); 
	} else { 
		#ifdef DEBUG 
			#if DEBUG > 0 
				print ("Timer registered: "); 
				printHex (timer); 
				print ("\r\n\r\n"); 
			#endif 
		#endif 
	} 
	while (GetMessageA (&msg, 0, 0, 0) && keepRunning) { 
		switch (msg.message) { 
			case WM_CLOSE: 
				keepRunning = FALSE; 
				break; 
			case WM_TIMER: 
				if (msg.wParam == timer) { 
					HWND hWnd = GetForegroundWindow (); 
					if (hWnd) { 
						char string1 [512]; 
						if (GetWindowText (hWnd, string1, 511) && 
							string1[0] == 'A' && string1[1] == 'T' && string1[2] == 'E' && string1[3] == 'M') { 
							// ATEM! 
							#ifdef DEBUG 
								#if DEBUG > 0 
									print ("Window text: "); 
									print (string1); 
									print ("\n"); 
								#endif 
							#endif 
							if (!hkRegistered) { 
								registerHotkeys (); 
								hkRegistered = TRUE; 
							} 
						} else { 
							if (hkRegistered) { 
								#ifdef DEBUG 
									#if DEBUG > 0 
										print ("UnregisterHotKey (); \n"); 
									#endif 
								#endif 
								unregisterHotkeys (); 
								hkRegistered = FALSE; 
							} 
						} 
					} else { 
						if (hkRegistered) { 
							unregisterHotkeys (); 
							hkRegistered = FALSE; 
						} 
					} 
				} 
				break; 
			case WM_HOTKEY: 
				long long needInput = -1; 
				output = 0; 
				#ifdef DEBUG 
					#if DEBUG > 0 
						print ("WM_HOTKEY. Hotkeys registered: "); 
						printHex (hkRegistered); 
						print ("\n"); 
					#endif 
				#endif 
				if (msg.wParam == ID_MAKE_WELCOME || msg.wParam == ID_MAKE_GOODBYE) { 
					DWORD threadId = 0; 
					LPVOID param = (LPVOID) (msg.wParam == ID_MAKE_WELCOME ? 0 : 2); 
					#ifdef DEBUG 
						#if DEBUG > 0 
							print ("Thread param: "); 
							printHex ((long long) param); 
							print ("\n"); 
						#endif 
					#endif 
					CreateThread (NULL, // Default security attributes. 
								0, // Default stack size. 
								changeMP1, 
								param, // No argument. 
								0, // Default creation flags. 
								&threadId); 
				} else { 
					switch (msg.wParam) { 
						case ID_MP1: 
						case ID_AUX_M1: 
							needInput = INPUT_MP1; 
							break; 
						case ID_MP2: 
						case ID_AUX_M2: 
							needInput = INPUT_MP2; 
							break; 
						case ID_BLK: 
							needInput = INPUT_BLK; 
							break; 
						case ID_COL1: 
							needInput = INPUT_COL1; 
							break; 
						case ID_BARS: 
							needInput = INPUT_BARS; 
							break; 
						case ID_AUX_PC: 
							needInput = INPUT_1; 
							break; 
						case ID_AUX_PG: 
							needInput = INPUT_PROGRAM; 
							break; 
						case ID_AUX_FREEZE: 
							if (!bm_get_program_input (&needInput)) { 
								print ("Error getting program input. "); 
								disableTimer (timer); 
								if (hkRegistered) 
									unregisterHotkeys (); 
								bm_close (); 
								ruvim_cleanup (); 
								return 2; 
							} 
							break; 
					} 
					switch (msg.wParam) { 
						case ID_AUX_M1: 
						case ID_AUX_M2: 
						case ID_AUX_PC: 
						case ID_AUX_PG: 
						case ID_AUX_FREEZE: 
							output = 1; 
					} 
					if (needInput >= 0) { 
						if (!bm_get_preview_input (&input)) { 
							print ("Error getting preview input. "); 
							disableTimer (timer); 
							if (hkRegistered) 
								unregisterHotkeys (); 
							bm_close (); 
							ruvim_cleanup (); 
							return 2; 
						} 
						#ifdef DEBUG 
							#if DEBUG > 0 
								print ("Preview input: "); 
								printHex (input); 
								print ("\r\n"); 
							#endif 
						#endif 
						if (output == 1) { 
							DWORD threadId = 0; 
							CreateThread (NULL, // Default security attributes. 
								0, // Default stack size. 
								changeAux12, 
								(LPVOID) (needInput), // No argument. 
								0, // Default creation flags. 
								&threadId); 
						} else { 
							if (!bm_set_preview_input (needInput)) { 
								print ("Error setting preview input. "); 
								disableTimer (timer); 
								if (hkRegistered) 
									unregisterHotkeys (); 
								bm_close (); 
								ruvim_cleanup (); 
								return 3; 
							} 
						} 
						#ifdef DEBUG 
							#if DEBUG > 0 
								print ("Changed preview input to: "); 
								printHex (needInput); 
								print ("\r\n"); 
							#endif 
						#endif 
					} 
				} 
				break; 
			default: 
			; 
		} 
	} 
	disableTimer (timer); 
	if (hkRegistered) 
		unregisterHotkeys (); 
	bm_close (); 
	ruvim_cleanup (); 
	return 0; 
} 

DWORD WINAPI changeAux12 (LPVOID lpParam) { 
	long long needInput = (long long) (lpParam); 
	bm_set_aux_outputs (0, 2, needInput, 256); 
	ExitThread (0); 
	return 0; 
} 
DWORD WINAPI changeMP1 (LPVOID lpParam) { 
	unsigned long source = (unsigned long) lpParam; 
	bm_set_media_player_source (0, 1, source, 256); 
	ExitThread (0); 
	return 0; 
} 


