Welcome! 

I am Ruvim Kondratyev, and this is some code I wrote for accessing the BlackMagic API from 
the programming languages C and Assembly. I wrote this because my computer didn't have 
enough space and I didn't have enough time to install Visual Studio on my computer to do 
BlackMagic API the actual IDL way. I just wanted a quick fix, so I started by writing the 
assembly files to do the C++ COM stuff, and then added C code to my projects. 

The prerequisites for this is to install NASM and DMC. 


The Netwide Assembler (NASM) may be found here: 
http://www.nasm.us/

Please add the path to nasm/bin to your Windows PATH environment variable before running the build.bat 
files provided with this code. 


The Digital Mars C compiler (DMC) may be found here: 
http://www.digitalmars.com/download/freecompiler.html

Use the "Digital Mars C/C++ Compiler" link. The STLport is *NOT* required for this code. It is all 
in C, no C++ code what-so-ever. If you wish to use C++, use the BlackMagic IDL with Visual Studio. 

As with NASM, add the Digital Mars "bin" folder path to your PATH environment variable before proceeding. 


The folder structure is as usual: 
Headers		-> C header files (.h) to #include<> in your programs. 
Sources		-> C source files (.c), and assembly source files (.asm), to 
			build before you may build your own program. 
			The provided build.bat scripts show how to build and use these. 
Samples		-> Sample programs. 


The build.bat procedure goes as: 
- Create a folder, "TemporaryBuildFiles" 
- Assemble BlackMagic-Interface.asm into an OBJ. 
- Compile thread-search.c into an OBJ. 
- Compile the sample program, incorporating code from the above two OBJs. 
- Move the temporary *.map and *.obj files into the temporary folder. 


The sample programs are these: 
- ASM-VideoKeyer		-> Shows how to use assembly language to access the 
					BlackMagic C++ COM API. This one does not 
					use the BlackMagic-Interface OBJ because 
					it has a lot of calls to the BlackMagic 
					API, some of which I haven't actually 
					implemented in BlackMagic-Interface.ASM; 
					I just left it like that. 
- ASM-VideoKeyer-2		-> Same as VideoKeyer, but with additional functionality. 
					For example, this one implements the F9, 
					F10, and F11 hotkeys as shortcuts to the 
					Ctrl + F9, Ctrl + F10, Ctrl + F11 ones. 
					The hotkeys are: 
						-- F9 / Ctrl + F9: Turn ON the upstream KEY1, 
							and update its MASK LEFT value to do 
							a SWIPE in from LEFT to RIGHT. 
						-- F10 / Ctrl + F10: Perform the reverse of F9: 
							SWIPE out from RIGHT to LEFT, and 
							disable upstream KEY1. 
						-- F11 / Ctrl + F11: Toggle KEY1 ON/OFF, but 
							disable its MASK flag. 
						-- Ctrl + Alt + U: Put input 1 on aux outputs 1 and 2. 
						-- Ctrl + Alt + I: Put PGM on aux 1-2. 
						-- Ctrl + Alt + O: Put MP1 on aux 1-2. 
						-- Ctrl + Alt + P: Put MP2 on aux 1-2. 
- C-AdditionalHotkeys		-> The BlackMagic switchers come with ATEM software 
					control. This is a program in C that starts 
					the ATEM Software Control when you start 
					this program. It also restarts itself when you try to 
					start it again. When it detects that you are viewing 
					the ATEM remote on your computer, it registers some 
					hotkeys, and when you're looking at something else 
					(not the remote), it unregisters them so that you 
					may use those keys for typing. When you hit the 
					hotkeys from the ATEM window, it switches the 
					switcher preview to the input corresponding to 
					the hotkey you used. 
					The hotkeys are: 
						-- J: Put MP1 on the preview. 
						-- K: Put MP2 on the preview. 
						-- D: Put BLACK on the preview. 
						-- R: Put COLOR1 on the preview. 
						-- A: Put COLOR-BARS on the preview. 
						-- Y: Whatever's on PGM, put it on aux outputs 1 and 2. 
						-- U: Put input 1 on aux outputs 1 and 2 (our church's aux 1 and 2 go to our front/back projectors). 
						-- I: Put PGM on aux outputs 1 and 2. 
						-- O: Put MP1 on aux outputs 1 and 2. 
						-- P: Put MP2 on aux outputs 1 and 2. 
						-- W: Put media index 0 (for our church, it's the Welcome slide) into the MP1 slot. 
						-- S: Put media index 2 (for our church, it's the See you soon! slide) into the MP1 slot. 
- C-ChangeAux			-> Simple demonstration in C of how to change the aux outputs. 
- C-Inputs			-> Iterates through all the available inputs on the switcher, and displays the input type. 
- C-Read-Preview		-> Simple demonstration in C of how to read which input 
					is on the preview. 
